var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
//queremos que tome los ficheros de la release
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log(' Polymer :' + port);
//cuando el usuario llegue a la raiz
app.get("/", function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'))
    res.sendFile("index.html", {root:'.'});
})
